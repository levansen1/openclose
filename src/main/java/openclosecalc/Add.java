/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package openclosecalc;


public class Add extends Calculator{
    
    public Add(double operand1, double operand2){
        super(operand1,operand2);
    }
    
      public double add(Calculator calc){
        return calc.getOperand1() + calc.getOperand2();
        
    }
     
}
